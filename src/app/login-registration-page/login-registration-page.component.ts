import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../user.service";

@Component({
  selector: 'app-login-registration-page',
  templateUrl: './login-registration-page.component.html',
  styleUrls: ['./login-registration-page.component.css']
})

export class LoginRegistrationPageComponent implements OnInit {

  signInForm: FormGroup ;
  signUpForm: FormGroup ;
  auth!: Object;
  name!: string;
  email!: string;
  password!: string;
  logEmail!: string;
  logPassword!: string;

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService) {
    this.signInForm = this.fb.group({
      email: ['', Validators.email],
      password:['', [ Validators.required, Validators.minLength(6)]]
    });
    this.signUpForm = this.fb.group({
      email: (['', Validators.email]),
      name: (['', Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ])
    });
  }



  ngOnInit(): void {

  }
  signIn() {
    if(this.signInForm.valid){
      this.userService.logInService({
        "email": this.logEmail,
        "password": this.logPassword
      }).subscribe(data => {
        //let auth = data;
        if(data == 'Failure'){
          //alertify.error("Invalid data");
        }
        else {
          // localStorage.setItem('userId', data._id);
          // localStorage.setItem('userName', data.name);
          this.router.navigate(['/client']);
          this.userService.In();
        }
      });
    }
    else {
      //console.log('Invalid');
    }
  }

  signUp() {
    if(this.signUpForm.valid){
      this.userService.signUpService({
        "username": this.name,
        "email": this.email,
        "password": this.password
      }).subscribe(data => {
        let auth = data;
        if(data == 'Failure'){
          //alertify.error("Invalid data");
        }
        else {
          this.router.navigate(['/client']);
          this.userService.In();
        }
      });

    }
    else {
      console.log('Invalid');
    }
  }
}
