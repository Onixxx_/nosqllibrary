import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  books : any;
  search: string = "";

  constructor(private bookService: BookService) {
  }

  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe((res)=> {
      console.log(res);
      this.books = res;
    });
  }

  getBooksByGenre(genre: string){
    this.bookService.getBooksByGenre(genre).subscribe((res)=> {
      this.books = res;
    });
  }

  getBookByName(){
    if (this.search == ""){
      this.bookService.getAllBooks().subscribe((res)=> {
        this.books = res;
      });
    }
    else {
      this.bookService.getBookByName(this.search).subscribe((res)=> {
        console.log(this.search);
        
        this.books = res;
      })
    }
  }

  addBookToUser(book: any){
    
  }
}
