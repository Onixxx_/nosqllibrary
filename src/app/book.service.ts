import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private Http : HttpClient) { }

  getBooksByGenre(genre: string) {
    const params = new HttpParams().set('genre', genre);
    return this.Http.get('http://localhost:8000/books.php', { params });
  }

  getBookByName(name: string) {
    const params = new HttpParams().set('name', name);
    return this.Http.get('http://localhost:8000/books.php', { params });
  }

  getAllBooks(){
    return this.Http.get('http://localhost:8000/books.php');
  }

  getRecommendationBook(){

  }
}
