import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  SignIn: any;

  constructor(private Http : HttpClient) {
    this.SignIn = new BehaviorSubject(false);
  }
  In(){
    this.SignIn.next(true);
  }
  Out(){
    this.SignIn.next(false);
  }

  signUpService(data: { username: string; email: string; password: string; }) {
    return this.Http.post('http://localhost:8000/users.php', data);
   }

  logInService(data: { email: string; password: string; }){
    let params = new HttpParams().set('email', data.email).set('password', data.password);
    return this.Http.get('http://localhost:8000/users.php', { params });
  }
}
