import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CatalogComponent } from './catalog/catalog.component';
import { MyFrameComponent } from './my-frame/my-frame.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ClientPageComponent } from './client-page/client-page.component';
import { LoginRegistrationPageComponent } from './login-registration-page/login-registration-page.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    MyFrameComponent,
    HomePageComponent,
    ClientPageComponent,
    LoginRegistrationPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
