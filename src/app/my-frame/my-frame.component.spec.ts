import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFrameComponent } from './my-frame.component';

describe('MyFrameComponent', () => {
  let component: MyFrameComponent;
  let fixture: ComponentFixture<MyFrameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyFrameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
