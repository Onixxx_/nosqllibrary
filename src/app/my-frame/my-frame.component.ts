import {Component, OnInit} from '@angular/core';
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-my-frame',
  templateUrl: './my-frame.component.html',
  styleUrls: ['./my-frame.component.css']
})
export class MyFrameComponent implements OnInit {
  isSignIn: boolean = false;
  constructor(private userService: UserService, private router:Router) {
    this.userService.SignIn.subscribe( (sign: boolean) => {
      this.isSignIn = sign;
    })
  }

  ngOnInit(): void {
  }
  goToMyPage(){
    if(!this.isSignIn){
      this.router.navigate(['/login&registr']);
    }
    else {
      this.router.navigate(['/client']);
    }
  }
}
