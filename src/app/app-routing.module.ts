import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogComponent } from './catalog/catalog.component';
import { ClientPageComponent } from './client-page/client-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginRegistrationPageComponent } from './login-registration-page/login-registration-page.component';

const routes: Routes = [
  {path:"welcome", component: HomePageComponent},
  {path:"catalog", component: CatalogComponent},
  {path:"client", component: ClientPageComponent},
  {path:"login&registr", component: LoginRegistrationPageComponent},
  {path:"", redirectTo: "/welcome", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
